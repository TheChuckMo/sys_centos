#!/bin/bash

# Install libvirt
# echo installing libvirt
# sudo yum -yq install debootstrap perl libvirt

# Install lxc
# echo installing lxc
# sudo yum -yq install lxc 

# Install templates
# echo installing lxc templates
# sudo yum -yq install lxc-templates

# Install ansible
echo installing ansible
sudo yum -yq install ansible

# check services
# for service in libvirtd.service lxc.service
#   do
#     echo checking $service
#     systemctl is-enabled $service || sudo systemctl enable $service
#   done
