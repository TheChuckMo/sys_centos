#!/bin/bash

# Installing vagrant keys 
sudo yum -y install wget
mkdir -p  ~/.ssh 
cd ~/.ssh 
wget --no-check-certificate 'https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub' -O authorized_keys 
chown -R vagrant:vagrant ~/.ssh
chmod 700 ~/.ssh 
chmod 600 ~/.ssh/authorized_keys 


