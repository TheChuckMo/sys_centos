#!/bin/bash

# clean up data
sudo yum -y clean all

# make cache
sudo yum -y makecache

# perform upgrade
sudo yum -y upgrade
