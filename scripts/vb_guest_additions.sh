#!/usr/bin/bash
#
# In virtual box guest additions
#
sudo yum -y update kernel*
sudo yum -y localinstall https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum -y install gcc kernel-devel kernel-headers dkms make bzip2 perl

sudo mount /home/vagrant/VBoxGuestAdditions.iso /mnt

KERN_DIR=/usr/src/kernels/`uname -r`
export KERN_DIR

sudo -E /mnt/VBoxLinuxAdditions.run

sudo umount /mnt
rm VBoxGuestAdditions.iso
