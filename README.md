CentOS 7 Packer Build
=====================

Lab CentOS 7 base box built with packer for vagrant and VirtualBox. 

*libvirtd and lxc installed and services started

Build
=====

From base ISO
```bash
$ packer build -force -var-file=./vars/centos7_vars.json ./templates/centos7_build.json
```

Add to vagrant box list
```bash
$ vagrant box add --name 'thechuckmo/centos7lxc' --checksum centos7lxc-0.1.2.chksum --provider virtualbox --force centos7lxc-0.1.2.box
```

Resources
=========

  - [Virtual Box](https://www.virtualbox.org/wiki/Downloads)
  - [Vagrant](https://www.vagrantup.com/downloads.html)
  - [Packer from HashiCorp](https://www.packer.io/downloads.html)
  - [libvirt Virtualization API](http://libvirt.org/)
  - [LXC Linux Containers](https://linuxcontainers.org/)
